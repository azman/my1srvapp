<?php
require_once dirname(__FILE__).'/../include/config.php';
class Task {
	protected function throw_debug($error) {
		throw new Exception('['.get_class($this).'] => {'.$error.'}');
	}
	protected function PrepSession() {
		require_once dirname(__FILE__).'/../include/Session.php';
		$that = new Session();
		return $that;
	}
	protected function PrepPage($name) {
		$code = dirname(__FILE__).'/../pages/'.$name.'.php';
		if (!file_exists($code))
			$this->throw_debug("Cannot find '".$name."'!");
		require_once $code;
		$page = new $name();
		return $page;
	}
	protected function ShowPage($name) {
		$page = $this->PrepPage($name);
		$page->Show();
	}
	function Run() {
		$this->throw_debug('Override this!');
	}
}
?>
