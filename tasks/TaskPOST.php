<?php
require_once dirname(__FILE__).'/Task.php';
class TaskPOST extends Task {
	function checkPass() {
		// all posts require a session?
		$session = $this->PrepSession();
		if (array_key_exists('postLogin',$_POST)) {
			if (!array_key_exists('username',$_POST)||
					!array_key_exists('password',$_POST))
				$this->throw_debug('Invalid login POST!');
			$session->StoreLogin($_POST['username'],$_POST['password']);
			header('Location:work.php');
			exit();
		}
		else if (array_key_exists('postChPass',$_POST)) {
			if (!array_key_exists('user',$_POST)||
					!array_key_exists('pass',$_POST)||
					!array_key_exists('pasX',$_POST)||
					!array_key_exists('pasY',$_POST))
				$this->throw_debug('Invalid chpass POST!');
			if ($_POST['pasX']!=$_POST['pasY'])
				$this->throw_debug('Mismatched password!');
			if ($session->Validate()!==true)
				$this->throw_debug('Invalid Session!');
			// change password - not authenticated here, just update session
			if ($session->CheckLogin($_POST['user'],$_POST['pass'])!==true)
				$this->throw_debug('Verification failed!');
			$this->ShowPage('PagePassCh');
		}
		else if (array_key_exists('postChNick',$_POST)) {
			if (!array_key_exists('cUnid',$_POST)||
					!array_key_exists('cName',$_POST)||
					!array_key_exists('cNick',$_POST))
				$this->throw_debug('Invalid chnick POST!');
			if ($session->Validate()!==true)
				$this->throw_debug('Invalid Session!');
			$this->ShowPage('PageNickCh');
		}
		// handle dummy post
		else if (array_key_exists('postDummy',$_POST)) {
			if (!array_key_exists('unid',$_POST))
				$this->throw_debug('Invalid dummy POST!');
			$page = $this->PrepPage('PageDone');
			$page->done_message("Dummy form OK?");
			$page->done_header("Dummy Form Test");
			$page->Show();
		}
	}
	function Run() {
		$this->checkPass();
		$this->throw_debug('Invalid POST!');
	}
}
?>
