<?php
require_once dirname(__FILE__).'/Task.php';
class TaskGET extends Task {
	function checkLogin() {
		// sessions only if using login mode
		$session = $this->PrepSession();
		if ($session->Validate()!==true) {
			// if no login info in this session, it's init or login request
			if (SHOW_INIT===false||isset($_GET['do'])&&$_GET['do']=='login')
				$page = PAGE_LOGIN;
			else {
				$session->Clean();
				$page = PAGE_INIT;
			}
			$this->ShowPage($page);
		}
	}
	function checkParam() {
		$test = [];
		if (isset($_GET['do'])) {
			if ($_GET['do']=='dopass') $this->ShowPage('PagePass');
			else if ($_GET['do']=='donick') $this->ShowPage('PageNick');
			else if ($_GET['do']=='login') unset($_GET['do']); // handled!
			else $test['get'] = 'do'; // let child classes try?
		}
		return $test;
	}
	function Run() {
		if (LOGIN_MODE===true)
			$this->checkLogin();
		$test = $this->checkParam();
		if (isset($test['get'])) {
			if ($test['get']!=='do')
				$this->throw_debug('Invalid \''.$test['get'].'\'!');
			// start checking
			if ($_GET['do']=='done') {
				$text = "Done.";
				if (isset($_GET['donetext']))
					$text = $_GET['donetext'];
				$page = $this->PrepPage('PageDone');
				$page->back_count();
				$page->done_message($text);
				if (isset($_GET['donehead']))
					$page->done_header($_GET['donehead']);
				$page->Show();
			}
		}
		foreach ($_GET as $key => $val)
			$this->throw_debug('Invalid option {'.$key.':'.$val.'}!');
		// if there are no parameters, show main page
		$this->ShowPage(PAGE_MAIN);
	}
}
?>
