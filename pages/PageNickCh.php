<?php
require_once dirname(__FILE__).'/PageDone.php';
class PageNickCh extends PageDone {
	function __construct() {
		$this->_title_ = 'Change Nickname';
		parent::__construct();
		// change nickname
		$this->_dodata->changeNick($_POST['cUnid'],$_POST['cNick']);
		// prepare page
		$user = $this->_dodata->getProfile();
		$this->_dotext = 'Nickname changed for '.$user['name'].'.';
		$this->_doskip = DOUBLE_BACK;
	}
}
?>
