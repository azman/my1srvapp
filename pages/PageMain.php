<?php
require_once dirname(__FILE__).'/../include/TraitBase.php';
require_once dirname(__FILE__).'/../include/TraitChart.php';
require_once dirname(__FILE__).'/Page.php';
class PageMain extends Page {
	use TraitBase;
	use TraitChart;
	function __construct() {
		parent::__construct();
		$this->initialize_base(LOGIN_MODE,DEFAULT_DATA_CLASS);
		$temp = $this->cssobj_chart();
		$this->_head->append_object($temp);
	}
	function build_page() {
		$view = $this->_doview;
		$view->insert_page_title();
		// custom main page
		$list = $view->menu_list_item_command(null,"donick","Change Nickname");
		$list = $view->menu_list_item_command($list,"dopass","Change Password");
		$list = $view->menu_list_item_link($list,"logout.php","Logout");
		$list = $view->menu_list_item_link($list,"work.php","Home");
		$user = $this->_validate?$this->_dodata->getProfile():null;
		$view->insert_menu($list,$user);
		$view->insert_page_section('Drawing using SVG');
		// show off svg stuffs!
		$svgd = new SVG(null,640,480);
		$svgg = $svgd->create_g($svgd,null);
		$svgg->transform("translate(60,60)");
		$svgx = $svgd->create_g($svgg,"x axis");
		$svgx->transform("translate(0,380)");
		// labels - 12 grades
		$lbls = array('A','A-','B+','B','B-','C+','C','C-','D+','D','D-','F');
		// loop through
		$xlen = 35; $xoff = 5 + $xlen;
		for ($loop=0,$xnxt=$xoff;$loop<count($lbls);$loop++,$xnxt+=$xoff) {
			$svgt = $svgd->create_g($svgx,"x tick");
			$svgt->transform("translate(".$xnxt.",0)");
			$svgt->draw_line(null,"0","6");
			$svgt->draw_text($lbls[$loop],null,"0","9",null,".71em");
		}
		$svgx->draw_path("domain","M0,0H560");
		$svgy = $svgd->create_g($svgg,"y axis");
		$svgy->transform("translate(0,-20)");
		// loop through
		$ylen = 40; $yoffs = 0;
		for ($loop=0,$ynxt=$yoffs;$loop<10;$loop++,$ynxt+=$ylen) {
			$svgt = $svgd->create_g($svgy,"y tick");
			$svgt->transform("translate(0,".$ynxt.")");
			$svgt->draw_line(null,"-6","0");
			$svgt->draw_text("".((10-$loop)*10)."%",null,"-9","0",null,".32em");
		}
		$svgy->draw_path("domain","M0,0V400");
		// draw bars
		$xnxt=$xoff-15;
		for ($loop=0;$loop<count($lbls);$loop++,$xnxt+=$xoff) {
			$svgg->draw_rectangle("bar",$xnxt,-20,$xlen,400);
		}
		$view->insert_block($svgd);
		$view->insert_highlight("This is a highlight!",
			["class"=>"w3-border w3-large w3-padding-small"]);
		$view->insert_page_section('Table Object/View');
		// create table - put these in view (theme class)!
		$ttab = $view->create_table();
		$view->insert_table($ttab);
		$view->create_table_header_row($ttab);
		$tcol = $view->create_table_header_col($ttab);
		$tcol->insert_inner("DATA1");
		$tcol = $view->create_table_header_col($ttab);
		$tcol->insert_inner("DATA2");
		for ($loop=0;$loop<4;$loop++) {
			$ttab->insert_data_row();
			$tcol = $ttab->insert_data_col();
			$tcol->insert_inner($loop."&nbsp;&nbsp;");
			$show = $view->create_badge($loop<<3,[ "class" => "w3-red" ]);
			$tcol->append_object($show);
			$tcol = $ttab->insert_data_col();
			$calc = $loop<<1;
			$text = $loop."&nbsp;&nbsp;";
			$tcol->insert_inner($text);
			if ($calc==4) {
				$link = "done&donehead=Duped!&donetext=HAHA!";
				$text = 'DO NOT CLICK ME!';
				$show = $view->create_menu_item($link,$text,
					MENUITEM_CMD_,[ "color" => "w3-green" ]);
				$tcol->append_object($show);
			}
		}
		$view->insert_page_section('File upload form');
		// create form
		$form = $view->create_form('form_command');
		$form->insert_keyvalue('enctype','multipart/form-data');
		$view->create_form_input_hidden($form,'unid',null,$user['unid']);
		$list = [];
		array_push($list,["Option 1","opt1",true]);
		array_push($list,["Option 2","opt2",false]);
		$temp = $view->create_form_select($form,'Options:','pickOpt',
			$list,["lbrk"=>true,"linebr"=>"2"]);
		$view->create_form_select_option($form,$temp,'Test 1','chkTest');
		$view->create_form_label($form,'Picks:&nbsp;&nbsp;');
		$view->create_form_input_checkbox($form,
			'&nbsp;Pick 1&nbsp;&nbsp;','pck1',["chkd"=>true]);
		$view->create_form_input_checkbox($form,
			'&nbsp;Pick 2','pck2',["linebr"=>"2"]);
		$view->create_form_input_file($form,'Data File','dataFile',
			["linebr"=>"1"]);
		$list = [];
		array_push($list,[" Pick 1","optRAD","rad1"]);
		array_push($list,[" Pick 2","optRAD","rad2"]);
		$view->create_form_input_radio_group($form,'Radio:','pickRAD',
			$list,["lbrk"=>true,"linebr"=>"2"]);
		$view->create_form_textarea($form,'Text Playground:','textBase',
			["lbrk"=>true,"linebr"=>"1"]);
		$view->create_form_label($form,'This does nothing!',["linebr"=>"1"]);
		$view->create_form_submit($form,'Dummy Post','postDummy');
		$view->insert_form($form,["fwpct"=>"33"]);
		$view->insert_page_section('Showing README');
		// read file and show it!
		require_once dirname(__FILE__).'/../include/FileText.php';
		$text = new FileText("README_MY1SRVAPP",dirname(__FILE__)."/..");
		$buff = $text->file_read();
		$temp = $this->make_span(nl2br($buff,false));
		$temp->insert_style("font-family:Courier;color:red");
		$view->insert_block($temp);
		// insert menu with dummy link!
		$list = $this->_doview->menu_list_item_command(null,
			"done&donehead=Tell me...&donetext='X' marks the spot!",
			'Show me the money!');
		$view->insert_menu($list);
	}
}
?>
