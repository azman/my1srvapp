<?php
require_once dirname(__FILE__).'/Page.php';
class PageTest extends Page {
	function __construct() {
		parent::__construct();
	}
	function build_page() {
		// use parent build
		parent::build_page();
		$view = $this->_doview;
		$view->insert_highlight("Adding to parent's build_page!");
	}
}
?>
