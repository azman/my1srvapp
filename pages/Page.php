<?php
/**
 * Page.php
 * - base class for all served pages
**/
require_once dirname(__FILE__).'/../include/config.php';
require_once dirname(__FILE__).'/../include/HTML.php';
class Page extends HTMLPage {
	protected $_title_; // in-page title
	protected $_theme_; // allow app to fix this!
	protected $_docmd_;
	protected $_prefix; // prefix for resources
	protected $_doview; // theme/viewer class
	function __construct($title=MY1APP_TITLE) {
		if (!isset($this->_title_))
			$this->_title_ = "";
		if (!isset($this->_theme_))
			$this->_theme_ = DEFAULT_THEME;
		if (!isset($this->_docmd_))
			$this->_docmd_ = "work.php?do=";
		if (!isset($this->_prefix))
			$this->_prefix = RESOURCE_PREFIX;
		parent::__construct($title);
		// find requested theme
		$pick = $this->_theme_;
		if ($pick=="Theme"||$pick=="ThemeI") // these are NOT for use
			$this->throw_debug("Not usable theme: ".$pick."!");
		$view = "themes/$pick.php";
		if (!file_exists($view))
			$this->throw_debug("Missing theme: ".$pick."!");
		include_once $view;
		if (!class_exists($pick))
			$this->throw_debug("Invalid theme: ".$pick."!");
		$this->_doview = new $pick($this);
		if (!$this->_doview instanceof ThemeI)
			$this->throw_debug("Not using ThemeI: ".$pick."!");
	}
	protected function throw_debug($error) {
		throw new Exception('['.get_class($this).'] => {'.$error.'}');
	}
	// custom text for page
	function text_command($acmd) {
		return $this->_docmd_.$acmd;
	}
	function text_link_back($back=DEFAULT_BACK_COUNT) {
		return 'javascript:history.go(-'.$back.')';
	}
	function text_page_title($title=null) {
		if ($title===null) $title = $this->_title_;
		if ($title!='') $title = ' - '.$title;
		return MY1APP_TITLE.$title;
	}
	// allow Theme* class to call these
	function insert_theme_style($that) {
		$temp = new HTMLObject('link',false);
		$temp->insert_keyvalue("rel","stylesheet");
		$temp->insert_keyvalue("href",$this->_prefix."styles/".$that);
		$temp->do_1skipline();
		$this->append_2head($temp);
		return $temp;
	}
	function insert_theme_script($that) {
		$temp = new HTMLObject('script');
		$temp->insert_keyvalue("type","text/javascript");
		$temp->insert_keyvalue("src",$this->_prefix."scripts/".$that);
		$temp->do_1skipline();
		$this->append_2body($temp);
		return $temp;
	}
	function insert_script_inline($text,$strict=true) {
		$temp = new HTMLObject('script');
		if ($strict) $text = "\"use strict\";\n".$text;
		$temp->insert_inner($text);
		$temp->do_1skipline();
		$this->append_2body($temp);
		return $temp;
	}
	// all pages call this
	function Show() {
		$this->write_html();
		exit();
	}
	// override this
	function build_page() {
		// only view classess should insert stuffs
		$view = $this->_doview;
		$view->insert_page_title();
		$view->insert_object($this->make_self_text("Default build_page!"));
	}
}
?>
