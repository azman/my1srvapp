<?php
require_once dirname(__FILE__).'/PageLogin.php';
class PagePass extends PageLogin {
	function __construct() {
		require_once dirname(__FILE__).'/../include/Session.php';
		$session = new Session();
		if ($session->Validate()!==true) {
			$this->throw_debug('Invalid Session!');
		}
		parent::__construct('Change Password');
	}
	function jsobj_main() {
		$js_main = <<< JS_MAIN
function post_check() {
	var chk_form = document.getElementById('form_chpass');
	chk_form.user.disabled = false;
	chk_form.pass.value = sha512(chk_form.pass.value);
	chk_form.pasX.value = sha512(chk_form.pasX.value);
	chk_form.pasY.value = sha512(chk_form.pasY.value);
	return true;
}
JS_MAIN;
		$jsobj = new JSObject('js_main');
		$jsobj->insert_inner($js_main);
		return $jsobj;
	}
	function build_page() {
		$view = $this->_doview;
		$view->insert_page_title();
		$temp = $this->jsobj_sha512lib();
		$this->append_2head($temp);
		$temp = $this->jsobj_main();
		$this->insert_2body($temp);
		// create form
		$form = $view->create_form('form_chpass');
		$form->insert_onsubmit('javascript:return post_check();');
		$view->create_form_input_text($form,'Username','user',
			[ "tval"=>$_SESSION[SESSION_USER] , "ro"=>1 , "linebr"=>"1" ]);
		$temp = $view->create_form_input_pass($form,'Old Password','pass',
			[ "hold"=>'Password' , "linebr"=>"1" ]);
		$temp->insert_constant("required");
		$temp = $view->create_form_input_pass($form,'New Password',
			'pasX', [ "hold"=>'New Password' , "linebr"=>"1" ]);
		$temp->insert_constant("required");
		$temp = $view->create_form_input_pass($form,'New Password (Again)',
			'pasY', [ "hold"=>'New Password (Again)' , "linebr"=>"1" ]);
		$temp->insert_constant("required");
		$view->create_form_submit($form,'Change','postChPass');
		$view->insert_form($form);
		$list = $view->menu_list_item_linkback(null);
		$view->insert_menu($list,false,["class"=>"w3-margin-left"]);
	}
}
?>
