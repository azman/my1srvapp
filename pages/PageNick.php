<?php
require_once dirname(__FILE__).'/../include/TraitBase.php';
require_once dirname(__FILE__).'/Page.php';
class PageNick extends Page {
	use TraitBase;
	function __construct() {
		$this->_title_ = 'Change Nickname';
		parent::__construct();
		$this->initialize_base(LOGIN_MODE,DEFAULT_DATA_CLASS);
	}
	function jsobj_main() {
		$js_main = <<< JS_MAIN
function mod_check() {
	var chk_form = document.getElementById('form_nick');
	chk_form.cName.disabled = false;
	return true;
}
JS_MAIN;
		$jsobj = new JSObject('js_main');
		$jsobj->insert_inner($js_main);
		return $jsobj;
	}
	function build_page() {
		$view = $this->_doview;
		$view->insert_page_title();
		$temp = $this->jsobj_main();
		$this->insert_2body($temp);
		$user = $this->_dodata->getProfile();
		// create form
		$form = $view->create_form('form_nick');
		$form->insert_onsubmit('javascript:return mod_check();');
		$view->create_form_input_hidden($form,'cUnid',null,$user['unid']);
		$temp = $view->create_form_input_text($form,'Full Name','cName',
			[ "tval"=>$user['name'] , "ro" => 1 , "linebr" => "1" ]);
		$temp = $view->create_form_input_text($form,'Nick Name','cNick',
			[ "tval"=>$user['nick'] , "linebr" => "1" ]);
		$view->create_form_submit($form,'Change','postChNick');
		$view->insert_form($form);
		$list = $view->menu_list_item_linkback(null);
		$view->insert_menu($list,false,["class"=>"w3-margin-left"]);
	}
}
?>
