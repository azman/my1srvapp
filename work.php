<?php
require_once dirname(__FILE__)."/include/config.php";
if (DEBUG_MODE) {
	error_reporting(E_ALL);
	ini_set('display_errors','1');
}
try {
	switch ($_SERVER['REQUEST_METHOD']) {
	case 'POST': $task = TASK_POST; break;
	case 'GET': $task = TASK_GET; break;
	case 'PUT': case 'DELETE': default:
		throw new Exception("Invalid request method!");
	}
	require_once dirname(__FILE__)."/tasks/".$task.".php";
	$task = new $task();
	$task->Run();
	throw new Exception("Unknown flow!");
}
catch (Exception $err) {
	if (DEBUG_MODE||EMESG_MODE) $mesg = $err->getMessage();
	else $mesg = EMESG_MASK;
}
// if we are here, must be because of an exception!
try {
	// try use PageDone to maintain theme look!
	$that = dirname(__FILE__).'/pages/PageDone.php';
	if (!file_exists($that)) throw new Exception("PageDone missing!");
	require_once $that;
	$page = new PageDone("Error",false); // no database needed?
	$page->done_message($mesg);
	$page->Show();
}
catch (Exception $err) {
	if (DEBUG_MODE||EMESG_MODE) $mesg = $mesg."<br>".$err->getMessage();
	else $mesg = EMESG_MASK;
}
echo "<!DOCTYPE html><html><body><h1>**ERROR**</h1>";
echo "<p>Message:{".$mesg."}</p>";
echo "<p>".MY1APP_TITLE." may not be correctly installed.</p>";
echo "</body></html>";
exit();
?>
