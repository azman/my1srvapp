<?php
/**
 * HTML.php
 * - classes to help serve HTML pages
**/
//------------------------------------------------------------------------------
class HTMLObject {
	protected $_uuid; // id tag
	protected $_ttag; // html tag
	protected $_init;
	protected $_tail;
	protected $_text;
	protected $_fore;
	protected $_subs;
	protected $_dobr;
	protected $_line;
	protected $_next;
	function __construct($tag=null,$end=true) {
		$this->_uuid = null;
		$this->_ttag = $tag;
		$this->_init = "";
		$this->_tail = "";
		if ($tag!==null) {
			$this->_init = "<".$tag.">";
			if ($end===true)
				$this->_tail = "</".$tag.">";
		}
		$this->_text = "";
		$this->_fore = true;
		$this->_subs = array();
		$this->_dobr = "";
		$this->_line = "";
		$this->_next = "";
	}
	function id() {
		return $this->_uuid;
	}
	function tag() {
		return $this->_ttag;
	}
	function remove_tail() {
		$this->_tail = "";
	}
	function do_multiline() {
		$this->_line = "\n";
	}
	// in case needed to cancel multiline :p
	function do_oneliner() {
		$this->_line = "";
	}
	// not needed when multilined
	function do_1skipline($next=true) {
		if ($next) $this->_next = "\n";
		else $this->_next = "";
	}
	function insert_linebr($count=1) {
		if (!isset($count)||empty($count))
			$count = 1;
		for ($loop=0;$loop<$count;$loop++) {
			$this->_dobr = $this->_dobr."<br>";
		}
	}
	function remove_linebr() {
		$this->_dobr = "";
	}
	function insert_id($id) {
		$this->_uuid = $id;
		$this->_init = substr($this->_init,0,strrpos($this->_init,">")).
			" id=\"".$id."\">";
	}
	function get_class() {
		$find = [];
		if (preg_match('/class=".*"/',$this->_init,$find)) {
			$temp = explode('=',$find[0]);
			return [ $temp[0] => trim(html_entity_decode($temp[1]),'"') ];
		}
		return null;
	}
	function insert_class($class) {
		$temp = $this->get_class();
		if ($temp!==null) {
			$snap = preg_replace('/ class=".*"/','',$this->_init);
			if ($snap!==null) $this->_init = $snap;
			$class = $temp['class']." ".$class;
		}
		$this->_init = substr($this->_init,0,strrpos($this->_init,">")).
			" class=\"".$class."\">";
	}
	function insert_class_2child($class,$ctag=null) {
		// recursive insert
		foreach ($this->_subs as $item) {
			$ttag = $item->tag();
			if ($ctag===null||$ttag===$ctag)
				$item->insert_class($class);
			$item->insert_class_2child($class,$ctag);
		}
	}
	function get_style() {
		$find = [];
		if (preg_match('/style=".*"/',$this->_init,$find)) {
			$temp = explode('=',$find[0]);
			return [ $temp[0] => trim(html_entity_decode($temp[1]),'"') ];
		}
		return null;
	}
	function insert_style($style) {
		$temp = $this->get_style();
		if ($temp!==null) {
			$snap = preg_replace('/ style=".*"/','',$this->_init);
			if ($snap!==null) $this->_init = $snap;
			$style = $temp['style'].$style;
		}
		$this->_init = substr($this->_init,0,strrpos($this->_init,">")).
			" style=\"".$style."\">";
	}
	function insert_keyvalue($key,$value,$noquote=false) {
		$do_quote="\"";
		if (strpos($value,"\"")!==false) $do_quote="'";
		if (isset($noquote)&&$noquote==true) $do_quote="";
		$this->_init = substr($this->_init,0,strrpos($this->_init,">")).
			" ".$key."=".$do_quote.$value.$do_quote.">";
	}
	function insert_inner($text,$fore=true) { // optionally add AFTER child
		$this->_text = $text;
		$this->_fore = $fore;
	}
	function insert_constant($value) {
		$this->_init = substr($this->_init,0,strrpos($this->_init,">")).
			" ".$value.">";
	}
	function insert_object($html) {
		if (is_a($html,'HTMLObject')) {
			array_unshift($this->_subs,$html);
			return true;
		}
		else return false;
	}
	function append_object($html) {
		if (is_a($html,'HTMLObject')) {
			array_push($this->_subs,$html);
			return true;
		}
		else return false;
	}
	function find_object_by_id($id) {
		// recursive find!
		foreach ($this->_subs as $item) {
			$uuid = $item->id();
			if ($uuid!==null&&$uuid===$id)
				return $item;
			$find = $item->find_object_by_id(id);
			if ($find!==null) return $find;
		}
		return null;
	}
	function form_html() { // html without child!
		$text = $this->_init.$this->_line;
		$text = $text.$this->_text.$this->_line;
		$text = $text.$this->_tail.$this->_dobr;
		$text = $text.$this->_line.$this->_next;
		return $text;
	}
	function write_html() {
		// first put out start tag
		echo $this->_init.$this->_line;
		// by default put this before child
		if ($this->_fore&&$this->_text!=="")
			echo $this->_text.$this->_line;
		// iterate through child elements
		foreach ($this->_subs as $item)
			$item->write_html();
		// optionally, put it AFTER child
		if (!$this->_fore&&$this->_text!=="")
			echo $this->_text.$this->_line;
		// end tag
		echo $this->_tail;
		// line break
		echo $this->_dobr;
		// if multiline
		echo $this->_line;
		// requested newline
		echo $this->_next;
	}
}
//------------------------------------------------------------------------------
class HTMLNull extends HTMLObject {
	function __construct() {
		parent::__construct(null);
	}
}
//------------------------------------------------------------------------------
class HTMLInnerText extends HTMLObject {
	// do i need this?
	function __construct($text) {
		parent::__construct('');
		$this->_text = $text;
	}
	function write_html() {
		echo $this->_text;
	}
}
//------------------------------------------------------------------------------
class CSSObject extends HTMLObject {
	function __construct($id) {
		parent::__construct('style');
		if (isset($id)&&!empty($id))
			$this->insert_id($id);
		$this->insert_keyvalue('type','text/css');
		$this->do_multiline();
	}
	function append_css($text) {
		if ($this->_text=="") $this->_text = $text;
		else $this->_text = $this->_text."\n".$text;
	}
}
//------------------------------------------------------------------------------
class JSObject extends HTMLObject {
	function __construct($id) {
		parent::__construct('script');
		if (isset($id)&&!empty($id))
			$this->insert_id($id);
		$this->insert_keyvalue('type','text/javascript');
		$this->do_multiline();
	}
	function append_js($text) {
		if ($this->_text=="") $this->_text = $text;
		else $this->_text = $this->_text."\n".$text;
	}
}
//------------------------------------------------------------------------------
class HTMLHead extends HTMLObject {
	function __construct() {
		parent::__construct('head');
		// head is multi-line
		$this->do_multiline();
	}
	function insert_charset($cset='UTF-8') {
		$temp = new HTMLObject('meta',false);
		$temp->insert_keyvalue("charset",$cset);
		$temp->do_1skipline();
		$this->append_object($temp);
		return $temp;
	}
	function insert_favicon($link="images/favicon.ico") {
		$temp = new HTMLObject('link',false);
		$temp->insert_keyvalue("rel","icon");
		$temp->insert_keyvalue("type","image/x-icon");
		$temp->insert_keyvalue("href",$link);
		$temp->do_1skipline();
		$this->append_object($temp);
		return $temp;
	}
	function insert_title($title) {
		$temp = new HTMLObject('title');
		$temp->insert_inner($title);
		$temp->do_1skipline();
		$this->append_object($temp);
		return $temp;
	}
	function insert_viewport() {
		$temp = new HTMLObject('meta',false);
		$temp->insert_keyvalue("name","viewport");
		$temp->insert_keyvalue("content","width=device-width,initial-scale=1");
		$temp->do_1skipline();
		$this->append_object($temp);
		return $temp;
	}
}
//------------------------------------------------------------------------------
class HTMLBody extends HTMLObject {
	function __construct() {
		parent::__construct('body');
		// body is multi-line
		$this->do_multiline();
	}
}
//------------------------------------------------------------------------------
class HTMLInputLabel extends HTMLObject {
	function __construct($text,$name=null) {
		parent::__construct("label");
		$this->insert_inner($text);
		if ($name!==null) // $name should be id for $text
			$this->insert_keyvalue('for',$name);
	}
}
//------------------------------------------------------------------------------
class HTMLInput extends HTMLObject {
	protected $_ilbl; // HTMLInputLabel object
	protected $_post; // flag place label AFTER input if true
	protected $_lbrk; // flag linebreak in between label/input pair
	function __construct($type,$name,$uuid=null,$vval=null) {
		parent::__construct("input",false);
		$this->_ilbl = null;
		$this->_post = false;
		$this->_lbrk = true;
		$this->insert_keyvalue('type',$type);
		if ($name!==null)
			$this->insert_keyvalue('name',$name);
		if ($uuid!==null)
			$this->insert_id($uuid);
		if ($vval!==null)
			$this->insert_keyvalue('value',$vval);
		$this->remove_tail(); // no end tag
	}
	function set_value($vval) {
		$find = [];
		if (preg_match('/value=".*"/',$this->_init,$find)) {
			$snap = preg_replace('/ value=".*"/','',$this->_init);
			if ($snap!==null) $this->_init = $snap;
		}
		$this->insert_keyvalue('value',$vval);
	}
	function get_label() { // do i need this?
		return $this->_ilbl;
	}
	function set_label($text,$post=false,$lbrk=true) {
		$uuid = $this->_uuid;
		$this->_ilbl = new HTMLInputLabel($text,$uuid);
		$this->_post = $post;
		$this->_lbrk = $lbrk;
		if ($this->_lbrk) {
			if ($this->_post) $this->insert_linebr();
			else $this->_ilbl->insert_linebr();
		}
		return $this->_ilbl;
	}
	function write_html() {
		if ($this->_post) $this->_ilbl->do_1skipline();
		else $this->do_1skipline();
		if ($this->_ilbl===null) $ilbl = new HTMLNull();
		else $ilbl = $this->_ilbl;
		if (!$this->_post) $ilbl->write_html();
		parent::write_html();
		if ($this->_post) $ilbl->write_html();
	}
}
//------------------------------------------------------------------------------
class HTMLFormBase extends HTMLObject {
	protected $_make_append;
	function __construct($type='form') {
		parent::__construct($type);
		$this->do_multiline();
		$this->_make_append = true;
	}
	function make_append($do=true) {
		$this->_make_append = $do;
	}
	function make_input_text($text,$name,$uuid=null,$tval=null,$opts=[]) {
		$temp = new HTMLInput('text',$name,$uuid,$tval);
		if (array_key_exists("hold",$opts))
			$temp->insert_keyvalue('placeholder',$opts['hold']);
		if (array_key_exists("ro",$opts)) {
			if (intval($opts['ro'])===1)
				$temp->insert_constant('disabled');
		}
		$temp->set_label($text);
		if ($this->_make_append===true)
			$this->append_object($temp);
		return $temp;
	}
	function make_input_pass($text,$name,$uuid=null,$tval=null,$opts=[]) {
		$temp = new HTMLInput('password',$name,$uuid,$tval);
		if (array_key_exists("hold",$opts))
			$temp->insert_keyvalue('placeholder',$opts['hold']);
		if (array_key_exists("ro",$opts)) {
			if (intval($opts['ro'])===1)
				$temp->insert_constant('disabled');
		}
		$temp->set_label($text);
		if ($this->_make_append===true)
			$this->append_object($temp);
		return $temp;
	}
	function make_input_hidden($name,$uuid=null,$tval=null) {
		$temp = new HTMLInput('hidden',$name,$uuid,$tval);
		if ($this->_make_append===true)
			$this->append_object($temp);
		return $temp;
	}
	function make_input_radio($text,$name,$uuid=null,$vval=null,$chkd=false) {
		$temp = new HTMLInput('radio',$name,$uuid,$vval);
		if ($chkd===true) $temp->insert_constant('checked');
		$temp->set_label($text,true,""); // after input, no linebr
		if ($this->_make_append===true)
			$this->append_object($temp);
		return $temp;
	}
	function make_input_checkbox($text,$name,$uuid=null,$chkd=false) {
		$temp = new HTMLInput('checkbox',$name,$uuid);
		if ($chkd===true) $temp->insert_constant('checked');
		$temp->set_label($text,true,""); // after input, no linebr
		if ($this->_make_append===true)
			$this->append_object($temp);
		return $temp;
	}
	function make_input_file($text,$name,$uuid=null) {
		$temp = new HTMLInput('file',$name,$uuid);
		$temp->set_label($text);
		if ($this->_make_append===true)
			$this->append_object($temp);
		return $temp;
	}
	function make_input_submit($vval,$name=null,$uuid=null) {
		$temp = new HTMLInput('submit',$name,$uuid,$vval);
		if ($this->_make_append===true)
			$this->append_object($temp);
		return $temp;
	}
	function make_select_option(&$psel,$text,$name,$chkd=false) {
		// $text=label, $name=name, $chkd=checked
		if (!is_a($psel,'HTMLObject')||$psel->tag()!=="select")
			return null;
		$that = new HTMLObject('option');
		$that->insert_keyvalue('value',$name);
		$that->insert_inner($text);
		$that->do_1skipline();
		if ($chkd===true)
			$that->insert_constant('selected');
		$psel->append_object($that);
		return $that;
	}
	function make_select($name,$uuid=null,$list=[]) {
		// opt = [ label, name, checked ]
		$temp = new HTMLObject('select');
		$temp->insert_keyvalue('name',$name);
		if ($uuid!==null) $temp->insert_id($uuid);
		$temp->do_multiline();
		// create list of options if requested
		foreach ($list as $item)
			$this->make_select_option($temp,$item[0],$item[1],$item[2]);
		if ($this->_make_append===true)
			$this->append_object($temp);
		return $temp;
	}
	function make_label($text,$lbrk=true) {
		$temp = new HTMLObject('label');
		$temp->insert_inner($text);
		if ($lbrk===true) $temp->insert_linebr();
		$temp->do_1skipline();
		if ($this->_make_append===true)
			$this->append_object($temp);
		return $temp;
	}
	function make_textarea($name,$form=null) {
		$temp = new HTMLObject('textarea');
		$temp->insert_keyvalue('name',$name);
		if ($form!==null)
			$temp->insert_keyvalue('form',$form);
		$temp->do_1skipline();
		if ($this->_make_append===true)
			$this->append_object($temp);
		return $temp;
	}
}
//------------------------------------------------------------------------------
class HTMLForm extends HTMLFormBase {
	function __construct($id,$action) {
		parent::__construct(); // form, by default
		$this->insert_id($id);
		$this->insert_keyvalue('method','POST');
		$this->insert_keyvalue('action',$action);
	}
	function insert_onsubmit($handler) {
		$this->insert_keyvalue('onsubmit',$handler);
	}
}
//------------------------------------------------------------------------------
class HTMLTable extends HTMLObject {
	protected $_head;
	protected $_body;
	protected $_foot;
	// keep current row for append_col
	protected $_hrow;
	protected $_irow;
	protected $_frow;
	function __construct() {
		parent::__construct('table');
		$this->do_multiline();
		$this->_head = null;
		$this->_body = null;
		$this->_foot = null;
		$this->_hrow = null;
		$this->_irow = null;
		$this->_frow = null;
	}
	function make_row($type='tr') {
		$trow = new HTMLObject($type);
		$trow->do_1skipline();
		return $trow;
	}
	function make_col($type='td') {
		$tcol = new HTMLObject($type);
		return $tcol;
	}
	function insert_header_row() {
		if ($this->_head==null) {
			/* only ONE thead */
			$this->_head = new HTMLObject('thead');
			$this->_head->do_multiline();
			$this->append_object($this->_head);
		}
		$this->_hrow = $this->make_row();
		$this->_head->append_object($this->_hrow);
		return $this->_hrow;
	}
	function insert_header_col() {
		if ($this->_hrow==null)
			$this->insert_header_row();
		$tcol = $this->make_col('th');
		$this->_hrow->append_object($tcol);
		return $tcol;
	}
	function insert_data_row() {
		if ($this->_body==null) {
			$this->_body = new HTMLObject('tbody');
			$this->_body->do_multiline();
			$this->append_object($this->_body);
		}
		$this->_irow = $this->make_row();
		$this->_body->append_object($this->_irow);
		return $this->_irow;
	}
	function insert_data_col() {
		if ($this->_irow==null)
			$this->insert_data_row();
		$tcol = $this->make_col();
		$this->_irow->append_object($tcol);
		return $tcol;
	}
	function insert_footer_row() {
		if ($this->_foot==null) {
			$this->_foot = new HTMLObject('tfoot');
			$this->_foot->do_multiline();
			$this->append_object($this->_foot);
		}
		$this->_frow = $this->make_row();
		$this->_foot->append_object($this->_frow);
		return $this->_frow;
	}
	function insert_footer_col() {
		if ($this->_frow==null)
			$this->insert_footer_row();
		$tcol = $this->make_col();
		$this->_frow->append_object($tcol);
		return $tcol;
	}
}
//------------------------------------------------------------------------------
class SVG_g extends HTMLObject {
	function __construct($class=null,$oneliner=false) {
		parent::__construct('g');
		if ($oneliner!==true)
			$this->do_multiline();
		if ($class!==null)
			$this->insert_class($class);
	}
	function transform($transform) {
		$this->insert_keyvalue("transform",$transform);
	}
	function draw_rectangle($class,$x,$y,$width,$height) {
		$rect = new HTMLObject('rect');
		if ($this->_line!=="") $rect->do_1skipline();
		if ($class!==null) $rect->insert_class($class);
		$rect->insert_keyvalue("x",$x);
		$rect->insert_keyvalue("y",$y);
		$rect->insert_keyvalue("width",$width);
		$rect->insert_keyvalue("height",$height);
		$this->append_object($rect);
	}
	function draw_text($tmsg,$class,$x,$y,$dx=null,$dy=null) {
		$text = new HTMLObject('text');
		if ($this->_line!=="") $text->do_1skipline();
		if ($class!==null) $text->insert_class($class);
		$text->insert_keyvalue("x",$x);
		$text->insert_keyvalue("y",$y);
		if ($dx!==null) $text->insert_keyvalue("dx",$dx);
		if ($dy!==null) $text->insert_keyvalue("dy",$dy);
		$text->insert_inner($tmsg);
		$this->append_object($text);
	}
	function draw_line($class,$x,$y) {
		$line = new HTMLObject('line');
		if ($this->_line!=="") $line->do_1skipline();
		if ($class!==null) $line->insert_class($class);
		$line->insert_keyvalue("x2",$x);
		$line->insert_keyvalue("y2",$y);
		$this->append_object($line);
	}
	function draw_path($class,$vector) {
		$path = new HTMLObject('path');
		if ($this->_line!=="") $path->do_1skipline();
		if ($class!==null) $path->insert_class($class);
		$path->insert_keyvalue("d",$vector);
		$this->append_object($path);
	}
}
//------------------------------------------------------------------------------
class SVG extends HTMLObject {
	protected $_1liner;
	function __construct($id=null,$width=null,$height=null,$oneliner=null) {
		parent::__construct('svg');
		if ($oneliner!==true) $this->_1liner = false;
		else $this->_1liner = true;
		if ($this->_1liner!==true)
			$this->do_multiline();
		if ($id!==null)
			$this->insert_id($id);
		if ($width!==null)
			$this->insert_keyvalue("width",$width);
		if ($height!==null)
			$this->insert_keyvalue("height",$height);
	}
	function create_g($parent,$class=null) {
		$svgg = new SVG_g($class,$this->_1liner);
		$parent->append_object($svgg);
		return $svgg;
	}
}
//------------------------------------------------------------------------------
class HTMLDocument extends HTMLObject {
	protected $_head;
	protected $_body;
	function __construct() {
		parent::__construct('html');
		// HTML document is ALWAYS multiline
		$this->do_multiline();
		// basically have these two only
		$this->_head = new HTMLHead();
		$this->append_object($this->_head);
		$this->_body = new HTMLBody();
		$this->append_object($this->_body);
	}
	function insert_2head($html) {
		return $this->_head->insert_object($html);
	}
	function insert_2body($html) {
		return $this->_body->insert_object($html);
	}
	function insert_onload($fnname) {
		return $this->_body->insert_keyvalue('onload',$fnname);
	}
	function append_2head($html) {
		return $this->_head->append_object($html);
	}
	function append_2body($html) {
		return $this->_body->append_object($html);
	}
	function make_object($otag,$multiline=true) {
		$temp = new HTMLObject($otag);
		if ($multiline) $temp->do_multiline();
		return $temp;
	}
	function write_html() {
		echo "<!DOCTYPE html>\n"; // html5!
		parent::write_html();
	}
}
//------------------------------------------------------------------------------
class HTMLPage extends HTMLDocument {
	function __construct($title=MY1APP_TITLE) {
		parent::__construct();
		$this->_head->insert_title($title);
		$this->_head->insert_charset();
		$this->_head->insert_viewport();
	}
	// object makers
	function make_div($skip=true) {
		$temp = new HTMLObject('div');
		if ($skip===true) $temp->do_1skipline();
		return $temp;
	}
	function make_link($link,$text) {
		$temp = new HTMLObject('a');
		$temp->insert_keyvalue('href',$link);
		if ($text!==null) $temp->insert_inner($text);
		return $temp;
	}
	function make_para($text,$endl=true) {
		$temp = new HTMLObject('p');
		if ($text!==null) $temp->insert_inner($text);
		if ($endl===true) $temp->do_1skipline();
		return $temp;
	}
	function make_span($text,$endl=true) {
		$temp = new HTMLObject('span');
		if ($text!==null) $temp->insert_inner($text);
		if ($endl===true) $temp->do_1skipline();
		return $temp;
	}
	function make_hobject($title=null,$level='h1') {
		$temp = new HTMLObject($level);
		$temp->insert_inner($title);
		return $temp;
	}
	function make_form($id,$action) {
		$temp = new HTMLForm($id,$action);
		return $temp;
	}
	function make_table() {
		$temp = new HTMLTable();
		return $temp;
	}
	// no need to override this
	final function write_html() {
		$this->build_page();
		parent::write_html();
	}
	// useful for debug?
	function make_self_text($text) {
		return "[".get_class($this)."] ".$text;
	}
	// override this!
	function build_page() {
		$this->insert_page_title();
		$temp = $this->make_div();
		$temp->insert_inner($this->make_self_text("This is HTML build page!"));
		$this->append_2body($temp);
	}
}
//------------------------------------------------------------------------------
?>
