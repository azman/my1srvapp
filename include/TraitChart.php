<?php
trait TraitChart  { // maybe use chartjs instead?
	function cssobj_chart() {
		$ccss = new CSSObject('css_chart');
		$ccss->do_multiline();
		$text = <<<CCSS
.bar {
  fill: #0000ff;
}
.bar:hover {
  fill: #8080ff;
}
.axis {
  font: 10px sans-serif;
}
.axis path,
.axis line {
  fill: none;
  stroke: #000;
  shape-rendering: crispEdges;
}
.tick {
  opacity: 1;
}
.x.tick text {
  text-anchor: middle;
}
.y.tick text {
  text-anchor: end;
}
.x.axis path {
//  display: none;
}
CCSS;
		$ccss->insert_inner($text);
		return $ccss;
	}
}
?>
