<?php
/**
 * Data.php
 * - base class for user data
**/
require_once dirname(__FILE__).'/Base.php';
class Data extends Base {
	protected $_userid;
	protected $_usrtab;
	protected $_dounid;
	protected $_dobkid;
	protected $_doname;
	protected $_donick;
	protected $_dotype;
	protected $_doflag;
	function __construct($lite=true) {
		parent::__construct($lite===true?DEFAULT_DBPATH:null);
		$this->make();
		$this->init();
		$this->_userid = null;
		$this->_usrtab = MY1APP_USERS_TABLE;
		$this->_dounid = null;
		$this->_dobkid = null;
		$this->_doname = null;
		$this->_donick = null;
		$this->_dotype = USER_NOT;
	}
	function is_lite() {
		return $this->_dblite;
	}
	function validate($username, $userpass) {
		// hashing done by clients
		$prep = "SELECT id,unid,bkid,name,nick,type,flag FROM ".
			$this->_usrtab." WHERE unid=? AND pass=? AND type!=0";
		$stmt = $this->prepare($prep);
		if (!$stmt->bindValue(1,$username,PDO::PARAM_STR)||
				!$stmt->bindValue(2,$userpass,PDO::PARAM_STR))
			$this->throw_this('validateUser bind error!');
		if (!$stmt->execute())
			$this->throw_this('validateUser execute error!');
		$item = $stmt->fetch(PDO::FETCH_ASSOC);
		if ($item==false) return false;
		$this->_userid = intval($item['id']); // make sure an integer?
		$this->_dounid = $item['unid'];
		$this->_dobkid = $item['bkid'];
		$this->_doname = $item['name'];
		$this->_donick = $item['nick'];
		$this->_dotype = intval($item['type']);
		$this->_doflag = intval($item['flag']);
		return true;
	}
	function getProfile() {
		return [ "id" => $this->_userid,
			"unid" => $this->_dounid, "bkid" => $this->_dobkid,
			"name" => $this->_doname, "nick" => $this->_donick,
			"type" => $this->_dotype, "flag" => $this->_doflag ];
	}
	function hashPass($passtext) {
		return hash('sha512',$passtext,false);
	}
	function resetPass($unid) {
		if ($this->_dotype!==USER_ADM)
			$this->throw_this('resetPass not-admin error!');
		if ($this->_dounid===$unid)
			$this->throw_this('resetPass own-pass error!');
		$prep = "SELECT id,bkid,name FROM ".$this->_usrtab." WHERE unid=?";
		$stmt = $this->prepare($prep);
		if (!$stmt->bindValue(1,$unid,PDO::PARAM_STR))
			$this->throw_this('resetPass bind error!');
		if (!$stmt->execute())
			$this->throw_this('resetPass execute error!');
		$item = $stmt->fetch(PDO::FETCH_ASSOC);
		if ($item===false)
			$this->throw_this('resetPass invalid user!');
		$stmt->closeCursor();
		$hash = $this->hashPass($item['bkid']);
		$prep = "UPDATE ".$this->_usrtab." SET pass=? WHERE id=?";
		$stmt = $this->prepare($prep);
		if (!$stmt->bindValue(1,$hash,PDO::PARAM_STR)||
				!$stmt->bindValue(2,$item['id'],PDO::PARAM_INT))
			$this->throw_this('resetPass bind2 error!');
		if (!$stmt->execute())
			$this->throw_this('resetPass execute2 error!');
		$stmt->closeCursor();
		unset($item['bkid']);
		return $item;
	}
	function modifyPass($username, $pass_old, $pass_new) {
		if ($this->_dounid!==$username)
			$this->throw_this('modifyPass not-own-pass error!');
		$prep = "SELECT id FROM ".$this->_usrtab." WHERE unid=? AND pass=?";
		$stmt = $this->prepare($prep);
		if (!$stmt->bindValue(1,$username,PDO::PARAM_STR)||
				!$stmt->bindValue(2,$pass_old,PDO::PARAM_STR))
			$this->throw_this('modifyPass bind error!');
		if (!$stmt->execute())
			$this->throw_this('modifyPass execute error!');
		$item = $stmt->fetch(PDO::FETCH_ASSOC);
		if ($item==false)
			$this->throw_this('modifyPass validate error!');
		$stmt->closeCursor();
		$prep = "UPDATE ".$this->_usrtab." SET pass=? WHERE id=?";
		$stmt = $this->prepare($prep);
		if (!$stmt->bindValue(1,$pass_new,PDO::PARAM_STR)||
				!$stmt->bindValue(2,$item['id'],PDO::PARAM_INT))
			$this->throw_this('modifyPass bind2 error!');
		if (!$stmt->execute())
			$this->throw_this('modifyPass execute2 error!');
		$stmt->closeCursor();
	}
	function changeNick($username, $nick_new) {
		if ($this->_dounid!==$username)
			$this->throw_this('changeNick not-own-nick error!');
		$prep = "UPDATE ".$this->_usrtab." SET nick=? WHERE unid=?";
		$stmt = $this->prepare($prep);
		if (!$stmt->bindValue(1,$nick_new,PDO::PARAM_STR)||
				!$stmt->bindValue(2,$username,PDO::PARAM_STR))
			$this->throw_this('changeNick bind error!');
		if (!$stmt->execute())
			$this->throw_this('changeNick execute error!');
		$stmt->closeCursor();
	}
	function checkUsers() {
		$table = $this->_usrtab;
		if (!$this->table_exists($table)) {
			$keyt = "INTEGER PRIMARY KEY";
			if ($this->_dblite===null) $keyt = $keyt." AUTO_INCREMENT";
			$tdata = array();
			array_push($tdata,
				array("name"=>"id","type"=>$keyt),
				array("name"=>"unid","type"=>"TEXT UNIQUE NOT NULL"),
				array("name"=>"bkid","type"=>"TEXT NOT NULL"),
				array("name"=>"pass","type"=>"TEXT NOT NULL"),
				array("name"=>"name","type"=>"TEXT NOT NULL"),
				array("name"=>"nick","type"=>"TEXT NOT NULL"),
				array("name"=>"type","type"=>"INTEGER"),
				array("name"=>"flag","type"=>"INTEGER"));
			$tmore = array();
			array_push($tmore,"UNIQUE (unid,bkid)");
			$this->table_create($table,$tdata,$tmore);
		}
	}
	function findUser($unid,$id=null) {
		$prep = "SELECT id,unid,bkid,name,nick,type,flag FROM ".$this->_usrtab;
		if ($id!==null) {
			$prep = $prep." WHERE id=?";
			$stmt = $this->prepare($prep);
			if (!$stmt->bindValue(1,$id,PDO::PARAM_INT))
				$this->throw_this('findUser bind1 error!');
		}
		else {
			$prep = $prep." WHERE unid=?";
			$stmt = $this->prepare($prep);
			if (!$stmt->bindValue(1,$unid,PDO::PARAM_STR))
				$this->throw_this('findUser bind2 error!');
		}
		if (!$stmt->execute())
			$this->throw_this('findUser execute error!');
		$pack = [];
		$item = $stmt->fetch(PDO::FETCH_ASSOC);
		if ($item!=false) {
			$pack = $item;
			$pack['stat'] = true;
			$pack['id'] = intval($item['id']); // make sure an integer?
			$pack['type'] = intval($item['type']);
			$pack['flag'] = intval($item['flag']);
		}
		else $pack['stat'] = false;
		return $pack;
	}
	function listUsers() {
		if ($this->_dotype!==USER_ADM&&$this->_dotype!==USER_MOD)
			$this->throw_this('listUsers not-admin/mod error!');
		$prep = "SELECT id,unid,bkid,name,nick,type,flag FROM ".$this->_usrtab.
			" WHERE type!=".USER_NOT." ORDER BY type,unid";
		$stmt = $this->prepare($prep);
		if (!$stmt->execute())
			$this->throw_this('listStaff execute error!');
		$pack = [];
		$list = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if ($list!=false) {
			$pack['stat'] = true;
			foreach ($list as &$item) {
				$item['id'] = intval($item['id']);
				$item['type'] = intval($item['type']);
				$item['flag'] = intval($item['flag']);
			}
			$pack['list'] = $list;
		}
		else $pack['stat'] = false;
		return $pack;
	}
	function createUser($unid,$bkid,$name,$nick,$type=USER_USR,$flag=0) {
		$hash = $this->hashPass($bkid);
		$type = intval($type);
		$flag = intval($flag);
		// find existing
		$item = $this->findUser($unid);
		if ($item['stat']===true)
			$this->throw_this('createUser exists error!');
		$prep = "INSERT INTO ".$this->_usrtab.
			" (unid,bkid,pass,name,nick,type,flag)".
			" VALUES (:unid,:bkid,:pass,:name,:nick,:type,:flag)";
		$stmt = $this->prepare($prep);
		$stmt->bindValue(':unid',$unid,PDO::PARAM_STR);
		$stmt->bindValue(':bkid',$bkid,PDO::PARAM_STR);
		$stmt->bindValue(':pass',$hash,PDO::PARAM_STR);
		$stmt->bindValue(':name',$name,PDO::PARAM_STR);
		$stmt->bindValue(':nick',$nick,PDO::PARAM_STR);
		$stmt->bindValue(':type',$type,PDO::PARAM_INT);
		$stmt->bindValue(':flag',$flag,PDO::PARAM_INT);
		if (!$stmt->execute())
			$this->throw_this('createUser execute error!');
		$stmt->closeCursor();
	}
	function removeUser($unid) {
		if ($this->_dotype!==USER_ADM)
			$this->throw_this('removeUser not-admin error!');
		// find existing
		$item = $this->findUser($unid);
		if ($item['stat']!==true)
			$this->throw_this('removeUser not-exist error!');
		$prep = "DELETE FROM ".$this->_usrtab." WHERE id=:id";
		$stmt = $this->prepare($prep);
		$stmt->bindValue(':id',$item['id'],PDO::PARAM_INT);
		if (!$stmt->execute())
			$this->throw_this('removeUser execute error!');
		$stmt->closeCursor();
	}
	function markUser($unid,$type) {
		if ($this->_dotype!==USER_ADM)
			$this->throw_this('markUser not-admin error!');
		if ($this->_dounid===$unid)
			$this->throw_this('markUser own-type error!');
		$type = intval($type);
		switch ($type) {
			case USER_ADM: case USER_MOD: case USER_USR: break;
			default: $type = USER_NOT;
		}
		// find existing
		$item = $this->findUser($unid);
		if ($item['stat']!==true)
			$this->throw_this('markUser exists error!');
		$prep = "UPDATE ".$this->_usrtab." SET type=? WHERE id=?";
		$stmt = $this->prepare($prep);
		if (!$stmt->bindValue(1,$type,PDO::PARAM_INT)||
				!$stmt->bindValue(2,$item['id'],PDO::PARAM_INT))
			$this->throw_this('markUser bind error!');
		if (!$stmt->execute())
			$this->throw_this('markUser execute error!');
		$stmt->closeCursor();
	}
	function doFlagUser($unid,$flag) {
		if ($this->_dotype!==USER_ADM)
			$this->throw_this('doFlagUser not-admin error!');
		if ($this->_dounid===$unid)
			$this->throw_this('doFlagUser own-flag error!');
		$flag = intval($flag);
		$item = $this->findUser($unid);
		if ($item['stat']!==true)
			$this->throw_this('doFlagUser exists error!');
		$prep = "UPDATE ".$this->_usrtab." SET flag=? WHERE id=?";
		$stmt = $this->prepare($prep);
		if (!$stmt->bindValue(1,$flag,PDO::PARAM_INT)||
				!$stmt->bindValue(2,$item['id'],PDO::PARAM_INT))
			$this->throw_this('doFlagUser bind error!');
		if (!$stmt->execute())
			$this->throw_this('doFlagUser execute error!');
		$stmt->closeCursor();
	}
	function isFlagUser($unid) {
		$item = $this->findUser($unid);
		if ($item['stat']!==true)
			$this->throw_this('isFlagUser exists error!');
		return $item['flag'];
	}
}
?>
