<?php
/**
 * install.php
 * - copy all configs into a local config
 * - installs a single user to start things up
**/
require_once dirname(__FILE__).'/include/config.php';
function config_local($here,$mdbf=false,$dbug=false) {
	if (file_exists($here)) {
		echo "## Reading ".$here.PHP_EOL;
		$text = file_get_contents($here);
		$new_ = false;
	} else {
		echo "## Creating new ".$here.PHP_EOL;
		$text = "<?php\n?>\n";
		$new_ = true;
	}
	$list = glob('include/config*.php',GLOB_BRACE);
	$find = "/^define_if_not_exists/";
	$tell = 0;
	foreach ($list as $conf) { // look up all config files
		$curr = 0;
		$temp = fopen($conf,'r');
		while (!feof($temp)) {
			$line = rtrim(fgets($temp,4096),"\r\n");
			if (preg_match($find,$line)) {
				$next = preg_replace($find,"//define",$line);
				//echo "-- Found:{".$next."}".PHP_EOL;
				$chk1 = strpos($next,"'");
				if ($chk1===false) continue; // assume invalid
				$chk1++;
				$chk2 = strpos($next,"'",$chk1);
				if ($chk2===false) continue;
				$tvar = substr($next,$chk1,$chk2-$chk1);
				//echo "-- Found:{".$tvar."}(".$chk1.")(".$chk2.")".PHP_EOL;
				$test = preg_match("|define\('".$tvar."',|",$text);
				if ($test===0) {
					if ($curr===0) {
						$curr++;
						echo "@@ From:{".basename($conf)."}".PHP_EOL;
					}
					echo "## Adding config '$tvar'".PHP_EOL;
					$text = preg_replace("/^(\?>)$/m","$next\n$1",$text);
					$tell = 1;
				}
				if ($tvar=="DEBUG_MODE"&&$dbug==true) {
					$old1 = "/^.*define\('".$tvar."',.*$/m"; // m=pcremultiline
					$new1 = "define('".$tvar."',true);";
					$text = preg_replace($old1,$new1,$text);
				} else if ($tvar=="DEFAULT_DBPATH"&&$mdbf==true) {
					$old1 = "/^.*define\('".$tvar."',.*$/m"; // m=pcremultiline
					$new1 = "define('".$tvar."',null);";
					$text = preg_replace($old1,$new1,$text);
				}
			}
		}
		fclose($temp);
	}
	if ($tell===1) {
		echo "-- Updating ".$here.PHP_EOL;
		file_put_contents($here,$text);
	}
	return $new_;
}
try {
	if (PHP_SAPI !== 'cli') { // or php_sapi_name()
		http_response_code(404);
		echo "<h1><p>Not found!</p></h1>".PHP_EOL;
		exit();
	}
	// defaults from default config.php
	$unid = DEFAULT_ROOT_UNID;
	$bkid = DEFAULT_ROOT_BKID;
	$name = DEFAULT_ROOT_NAME;
	$dbug = false;
	$mdbf = false; // mariadb flag
	// check parameter
	for ($loop=1;$loop<$argc;$loop++) {
		if ($argv[$loop]==='--unid'&&$loop<$argc-1) {
			$unid = $argv[++$loop];
		} else if ($argv[$loop]==='--bkid'&&$loop<$argc-1) {
			$bkid = $argv[++$loop];
		} else if ($argv[$loop]==='--name'&&$loop<$argc-1) {
			$name = $argv[++$loop];
		} else if ($argv[$loop]==='--mariadb') {
			$mdbf = true;
		} else if ($argv[$loop]==='--debug') {
			$dbug = true;
		} else {
			echo "Unknown option '".$argv[$loop]."'";
		}
	}
	// create local config
	$test = config_local(MY1CFGINIT,$mdbf,$dbug);
		// create database file if required
	if (LOGIN_MODE===true) {
		if (!$test) {
			// create first user
			$data = DEFAULT_DATA_CLASS;
			require_once dirname(__FILE__).'/include/'.$data.'.php';
			$test = new $data(true);
			$temp = $test->is_lite();
			if ($temp!==null) echo "@@ Lite:{".basename($temp)."}".PHP_EOL;
			else echo "@@ MariaDB!".PHP_EOL;
			echo "-- Checking data class {".$data."}".PHP_EOL;
			$test->checkUsers();
			echo "-- Finding initial user id {".$unid."}...";
			$init = $test->findUser($unid);
			echo "done. => ".json_encode($init).PHP_EOL;
			if ($init['stat']===false) {
				echo "-- Creating default admin account {".$unid."}...";
				$test->createUser($unid,$bkid,$name,$name,USER_ADM);
				echo "done!".PHP_EOL;
			}
		} else {
			echo "@@ Modify ".basename(MY1CFGINIT).
				" and re-run this script to install admin user!".PHP_EOL;
		}
	} else {
		echo "@@ Login NOT required! Nothing to install!".PHP_EOL;
	}
} catch( Exception $error ) {
	echo "Installation error! [".$error->getMessage()."]".PHP_EOL;
}
exit();
?>
