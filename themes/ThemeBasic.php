<?php
/**
 * ThemeBasic.php
 * - basic theme using w3css (and awesomefont?)
**/
require_once dirname(__FILE__).'/Theme.php';
require_once dirname(__FILE__).'/ThemeI.php';
class ThemeBasic extends Theme implements ThemeI {
	protected $_dohead; // page title object
	protected $_dobody; // container body for this theme
	protected $_Tcolor; // main theme color
	protected $_hcolor; // header color
	protected $_bcolor; // button color
	protected $_pcolor; // panel color
	function __construct($page=null) {
		parent::__construct($page);
		$this->_dohead = null;
		$this->_dobody = $this->create_panel(null,["id"=>"themebasic_body"]);
		$this->_page->append_2body($this->_dobody);
		if (!isset($this->_Tcolor))
			$this->_Tcolor = "blue"; // create a config for this?
		if (!isset($this->_hcolor))
			$this->_hcolor = "w3-theme-d2";
		if (!isset($this->_bcolor))
			$this->_bcolor = "w3-theme-d3";
		if (!isset($this->_pcolor))
			$this->_pcolor = "w3-theme-d5";
		// do this here?
		$this->_page->insert_theme_style("w3.min.css");
		$this->_page->insert_theme_style("w3-theme-".$this->_Tcolor.".css");
		// default style does not use icon? another theme?
		$this->_page->insert_theme_style("fontawesome.min.css");
		$this->_page->insert_theme_style("solid.min.css");
	}
	// from stuffs
	function create_form($uuid,$opts=[]) {
		$form = new HTMLForm($uuid,'work.php');
		if (array_key_exists("class",$opts))
			$form->insert_class($opts['class']);
		return $form;
	}
	function create_form_input_text($form,$flbl,$name,$opts=[]) {
		if (array_key_exists("uuid",$opts)) $uuid = $opts['uuid'];
		else $uuid = null;
		if (array_key_exists("tval",$opts)) $tval = $opts['tval'];
		else $tval = null;
		$pick = [];
		if (array_key_exists("hold",$opts)) $pick['hold'] = $opts['hold'];
		if (array_key_exists("ro",$opts)) $pick['ro'] = $opts['ro'];
		$temp = $form->make_input_text($flbl,$name,$uuid,$tval,$pick);
		$temp->insert_class("w3-input w3-border");
		// make this a function!
		if (array_key_exists("linebr",$opts)) {
			$pick = intval($opts['linebr']);
			if ($pick>0) $temp->insert_linebr($pick);
		}
		return $temp;
	}
	function create_form_input_pass($form,$flbl,$name,$opts=[]) {
		if (array_key_exists("uuid",$opts)) $uuid = $opts['uuid'];
		else $uuid = null;
		if (array_key_exists("tval",$opts)) $tval = $opts['tval'];
		else $tval = null;
		$pick = [];
		if (array_key_exists("hold",$opts)) $pick['hold'] = $opts['hold'];
		if (array_key_exists("ro",$opts)) $pick['ro'] = $opts['ro'];
		$temp = $form->make_input_pass($flbl,$name,$uuid,$tval,$pick);
		$temp->insert_class("w3-input w3-border");
		// make this a function!
		if (array_key_exists("linebr",$opts)) {
			$pick = intval($opts['linebr']);
			if ($pick>0) $temp->insert_linebr($pick);
		}
		return $temp;
	}
	function create_form_input_hidden($form,$name,$uuid,$vval) {
		$temp = $form->make_input_hidden($name,$uuid,$vval);
		return $temp;
	}
	function create_form_submit($form,$text,$name,$opts=[]) {
		$temp = $form->make_input_submit($text,$name);
		if (array_key_exists("bcolor",$opts))
			$pick = $opts['bcolor'];
		else $pick = $this->_bcolor;
		$temp->insert_class("w3-btn ".$pick);
		return $temp;
	}
	function create_form_input_radio($form,$text,$name,$opts=[]) {
		if (array_key_exists("uuid",$opts)) $uuid = $opts['uuid'];
		else $uuid = null;
		if (array_key_exists("vval",$opts)) $vval = $opts['vval'];
		else $vval = null;
		if (array_key_exists("chkd",$opts)) $chkd = $opts['chkd'];
		else $chkd = false;
		$temp = $form->make_input_radio($text,$name,$uuid,$vval,$chkd);
		$temp->insert_class("w3-radio");
		if (array_key_exists("linebr",$opts)) {
			$pick = intval($opts['linebr']);
			if ($pick>0) $temp->insert_linebr($pick);
		}
		return $temp;
	}
	function create_form_input_radio_group($form,$flbl,$name,$list,$opts=[]) {
		$what = $this->create_form_label($form,$flbl);
		if (array_key_exists("lbrk",$opts)&&$opts['lbrk']===true)
			$what->insert_linebr();
		//  [ label, value ]
		$loop = 0; $temp = null; $that = -1;
		if (array_key_exists("rsel",$opts))
			$that = intval($opts['rsel']);
		foreach ($list as $item) {
			$pick = [];
			$pick['vval'] = $item[1];
			$chkd = ($loop===$that) ? true : false;
			if ($chkd) $pick['chkd'] = $chkd;
			$temp = $this->create_form_input_radio($form,$item[0],$name,$pick);
			$loop++;
		}
		if ($temp!=null) {
			if (array_key_exists("linebr",$opts)) {
				$pick = intval($opts['linebr']);
				if ($pick>0) {
					$rlbl = $temp->get_label();
					$rlbl->insert_linebr($pick);
				}
			}
		}
	}
	function create_form_select($form,$flbl,$name,$list,$opts=[]) {
		$what = $this->create_form_label($form,$flbl);
		if (array_key_exists("lbrk",$opts)&&$opts['lbrk']===true)
			$what->insert_linebr();
		if (array_key_exists("uuid",$opts)) $uuid = $opts['uuid'];
		else $uuid = null;
		$temp = $form->make_select($name,$uuid,$list);
		$temp->insert_class("w3-select");
		if (array_key_exists("linebr",$opts)) {
			$pick = intval($opts['linebr']);
			if ($pick>0) $temp->insert_linebr($pick);
		}
		return $temp;
	}
	function create_form_select_option($form,$psel,$text,$name,$chkd=false) {
		$temp = $form->make_select_option($psel,$text,$name,$chkd);
		return $temp;
	}
	function create_form_input_file($form,$flbl,$name,$opts=[]) {
		if (array_key_exists("uuid",$opts)) $uuid = $opts['uuid'];
		else $uuid = null;
		$temp = $form->make_input_file($flbl,$name,$uuid);
		$temp->insert_class("w3-input w3-border");
		if (array_key_exists("linebr",$opts)) {
			$pick = intval($opts['linebr']);
			if ($pick>0) $temp->insert_linebr($pick);
		}
		return $temp;
	}
	function create_form_input_checkbox($form,$flbl,$name,$opts=[]) {
		if (array_key_exists("uuid",$opts)) $uuid = $opts['uuid'];
		else $uuid = $name;
		if (array_key_exists("chkd",$opts)) $chkd = $opts['chkd'];
		else $chkd = false;
		$temp = $form->make_input_checkbox($flbl,$name,$uuid,$chkd);
		$temp->insert_class("w3-check");
		if (array_key_exists("linebr",$opts)) {
			$pick = intval($opts['linebr']);
			if ($pick>0) {
				$ilbl = $temp->get_label();
				$ilbl->insert_linebr($pick);
			}
		}
		return $temp;
	}
	function create_form_label($form,$flbl,$opts=[]) {
		$temp = $form->make_label($flbl,false);
		if (array_key_exists("linebr",$opts)) {
			$pick = intval($opts['linebr']);
			if ($pick>0) $temp->insert_linebr($pick);
		}
		return $temp;
	}
	function create_form_textarea($form,$flbl,$name,$opts=[]) {
		$what = $this->create_form_label($form,$flbl);
		if (array_key_exists("lbrk",$opts)&&$opts['lbrk']===true)
			$what->insert_linebr();
		$temp = $form->make_textarea($name,null);
		$temp->insert_class("w3-input w3-border");
		$temp->insert_style("resize:none;width:100%;");
		if (array_key_exists("linebr",$opts)) {
			$pick = intval($opts['linebr']);
			if ($pick>0) $temp->insert_linebr($pick);
		}
		return $temp;
	}
	// table stuffs
	function create_table($opts=[]) {
		$ttab =  new HTMLTable();
		$ttab->insert_keyvalue('width','100%',true);
		$ttab->insert_keyvalue('cellpadding','10',true);
		$pick = "w3-table-all"; // "w3-table w3-border w3-bordered w3-striped"
		$pick = $pick." w3-hoverable";
		if (array_key_exists("class",$opts))
			$pick = $pick." ".$opts['class'];
		$ttab->insert_class($pick);
		return $ttab;
	}
	function create_table_header_row(&$ttab=null,$opts=[]) {
		if ($ttab===null) $ttab = $this->create_table();
		$trow = $ttab->insert_header_row();
		// force a color on header row
		if (array_key_exists("color",$opts))
			$pick = $opts['color'];
		else
			$pick = "w3-light-grey";
		if (array_key_exists("class",$opts))
			$pick = $pick." ".$opts['class'];
		$trow->insert_class($pick);
		return $trow;
	}
	function create_table_header_col(&$ttab=null,$opts=[]) {
		if ($ttab===null) $ttab = $this->create_table();
		$tcol = $ttab->insert_header_col();
		if (array_key_exists("class",$opts))
			$tcol->insert_class($opts['class']);
		return $tcol;
	}
	function create_table_data_row(&$ttab=null,$opts=[]) {
		if ($ttab===null) $ttab = $this->create_table();
		$trow = $ttab->insert_data_row();
		if (array_key_exists("color",$opts))
			$pick = " ".$opts['color'];
		else $pick = "";
		if (array_key_exists("class",$opts))
			$pick = $opts['class'].$pick;
		if ($pick!=="") $trow->insert_class($pick);
		return $trow;
	}
	function create_table_data_col(&$ttab=null,$opts=[]) {
		if ($ttab===null) $ttab = $this->create_table();
		$tcol = $ttab->insert_data_col();
		if (array_key_exists("class",$opts))
			$tcol->insert_class($opts['class']);
		return $tcol;
	}
	//---- Interface methods
	function create_panel($item=null,$opts=[]) {
		$temp = $this->_page->make_div(false);
		$temp->do_multiline();
		if (array_key_exists('id',$opts))
			$temp->insert_id($opts['id']);
		if (array_key_exists("class",$opts))
			$pick = $opts['class'];
		else $pick = "w3-container w3-padding-16";
		$temp->insert_class($pick);
		if ($item!==null) {
			if (is_a($item,'HTMLObject'))
				$temp->append_object($item);
			else $temp->insert_inner($item); // assume text
		}
		return $temp;
	}
	function create_menu_item($link,$text,$type=MENUITEM_LINK,$opts=[]) {
		if ($type===MENUITEM_CMD_)
			$link = $this->_page->text_command($link);
		// menu link
		$item = $this->_page->make_link($link,null);
		$item->do_1skipline();
		if (array_key_exists("color",$opts))
			$color = $opts['color'];
		else $color = $this->_bcolor;
		$pick = "w3-button w3-round-xxlarge w3-mobile ".$color;
		if (array_key_exists("class",$opts))
			$pick = $pick." ".$opts['class'];
		// menu label - button object for easing class-ing
		$butt = new HTMLObject('button');
		$butt->insert_keyvalue("type","button");
		$butt->insert_class($pick);
		$butt->insert_inner($text);
		$item->append_object($butt);
		return $item;
	}
	function create_badge($show,$opts=[]) {
		$pick = "w3-badge";
		if (array_key_exists("class",$opts))
			$pick = $pick." ".$opts['class'];
		$temp = $this->_page->make_span($show);
		$temp->do_1skipline();
		$temp->insert_class($pick);
		return $temp;
	}
	function insert_object($item,$opts=[]) {
		if (is_a($item,'HTMLObject')) $temp = $item;
		else {
			$temp = $this->_page->make_div(false); // no need 1skipline
			$temp->do_multiline(); // because we do multiline
			$pick = "w3-panel";
			if (array_key_exists("class",$opts))
				$pick = $pick." ".$opts['class'];
			$temp->insert_class($pick);
			$temp->insert_inner($item); // assume text
		}
		$this->_dobody->append_object($temp);
		return $temp;
	}
	function insert_page_title($title=null,$opts=[]) {
		// ensures ONE page title
		if ($this->_dohead!==null) return $this->_dohead;
		if (array_key_exists("color",$opts))
			$color = $opts['color'];
		else $color = $this->_hcolor;
		$pick = "w3-container ".$color; // w3-container has paddings
		if (array_key_exists("class",$opts))
			$pick = $pick." ".$opts['class'];
		if (array_key_exists("hlevel",$opts))
			$hlvl = $opts['class'];
		else $hlvl = 'h1';
		// container for page title
		$temp = new HTMLObject('header');
		$temp->insert_class($pick);
		$temp->do_multiline();
		// page header
		$text = $this->_page->text_page_title($title);
		$cont = $this->_page->make_span($text,false);
		$pack = $this->_page->make_hobject($cont->form_html(),$hlvl);
		$pack->do_1skipline();
		$temp->append_object($pack);
		// direct to page body
		$this->_page->insert_2body($temp);
		$this->_dohead = $temp;
		return $temp;
	}
	function insert_page_section($title,$opts=[]) {
		$pick = "w3-container w3-margin-top w3-margin-bottom";
		if (array_key_exists("color",$opts))
			$color = $opts['color'];
		else $color = $this->_pcolor;
		$pick = $pick." ".$color;
		if (array_key_exists("class",$opts))
			$opts['class'] = $pick." ".$opts['class'];
		else $opts['class'] = $pick;
		if (array_key_exists("hlevel",$opts))
			$hlvl = $opts['class'];
		else $hlvl = 'h3';
		// container for page section
		$temp = $this->_page->make_div(false);
		$temp->do_multiline();
		$pick = "w3-bar";
		if (array_key_exists("class",$opts))
			$pick = $pick." ".$opts['class'];
		$temp->insert_class($pick);
		// page section
		$cont = $this->_page->make_span($title,false);
		$pack = $this->_page->make_hobject($cont->form_html(),$hlvl);
		$pack->do_1skipline();
		$temp->append_object($pack);
		// put in theme body
		$this->_dobody->append_object($temp);
		return $temp;
	}
	function insert_block($item,$opts=[]) {
		$temp = $this->create_panel($item,$opts);
		$this->_dobody->append_object($temp);
		return $temp;
	}
	function insert_menu($list,$user=false,$opts=[]) {
		if (!is_array($list))
			$this->throw_this("No array for menu!");
		$menu = $this->_page->make_div(false);
		$menu->do_multiline();
		$pick = "w3-bar";
		if (array_key_exists("class",$opts))
			$pick = $pick." ".$opts['class'];
		$menu->insert_class($pick);
		foreach ($list as $item) {
			$that = $this->create_menu_item($item['that'],
				$item['text'],$item['type']);
			$menu->append_object($that);
		}
		if ($user!==false) {
			if ($user===null) $text = 'Hello, World!';
			else $text = 'Hello, '.$user['nick'];
			$temp = new HTMLObject('button');
			$pick = "w3-button w3-light-grey w3-text-black";
			$pick = $pick." w3-hover-blue w3-hover-text-white";
			$temp->insert_class($pick);
			$temp->do_1skipline();
			$temp->insert_inner($text);
			$menu->insert_object($temp); // put it first
		}
		$this->insert_object($menu);
		return $menu;
	}
	function insert_highlight($text,$opts=[]) {
		$temp = $this->_page->make_div(false);
		$temp->do_multiline();
		$pick = "w3-panel";
		if (array_key_exists("color",$opts))
			$pick = $pick." ".$opts['color'];
		if (array_key_exists("class",$opts))
			$pick = $pick." ".$opts['class'];
		$temp->insert_class($pick);
		$item = $this->_page->make_para("<b>".$text."</b>"); // auto-bold
		$temp->insert_object($item);
		$this->_dobody->append_object($temp);
		return $temp;
	}
	function insert_form($form,$opts=[]) {
		$wcls = "w3-third";
		$pick = "w3-container w3-padding-16 w3-card-4";
		$pick = $pick." w3-margin-left w3-margin-bottom";
		if (array_key_exists("fwpct",$opts)) {
			$size = intval($opts['fwpct']);
			if ($size>0&&$size<=100) {
				if ($size>75) $wcls = "w3-rest";
				else if ($size>67) $wcls = "w3-threequarter";
				else if ($size>50) $wcls = "w3-twothird";
				else if ($size>33) $wcls = "w3-half";
				else if ($size>25) $wcls = "w3-third";
				else $wcls = "w3-quarter";
			}
		}
		$pick = $pick." ".$wcls;
		if (array_key_exists("class",$opts))
			$opts['class'] = $opts['class']." ".$pick;
		else $opts['class'] = $pick;
		$temp = $this->insert_block($form,$opts);
		return $temp;
	}
	function insert_table($ttab,$opts=[]) {
		$pick = [];
		if (array_key_exists("class",$opts))
			$pick['class'] = $opts['class'];
		$temp = $this->insert_block($ttab,$pick);
		return $temp;
	}
}
?>
