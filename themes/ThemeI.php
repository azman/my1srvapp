<?php
interface ThemeI {
/*
cat Theme*.php | grep "function" | grep -E "insert_|create_" |
	sed -e "s/ {/;/" -e "s/,\$opts=\[\]//"
*/
	function create_form($uuid);
	function create_form_input_text($form,$flbl,$name);
	function create_form_input_pass($form,$flbl,$name);
	function create_form_input_hidden($form,$name,$uuid,$vval);
	function create_form_submit($form,$text,$name);
	function create_form_input_radio($form,$text,$name);
	function create_form_input_radio_group($form,$flbl,$name,$list);
	function create_form_select($form,$flbl,$name,$list);
	function create_form_select_option($form,$psel,$text,$name,$chkd);
	function create_form_input_file($form,$flbl,$name);
	function create_form_input_checkbox($form,$flbl,$name);
	function create_form_label($form,$flbl);
	function create_table();
	function create_table_header_row(&$ttab);
	function create_table_header_col(&$ttab);
	function create_table_data_row(&$ttab);
	function create_table_data_col(&$ttab);
	function create_panel($item);
	function create_menu_item($link,$text,$type);
	function create_badge($show);
	function insert_object($item);
	function insert_page_title($title);
	function insert_page_section($title);
	function insert_block($item);
	function insert_menu($list,$user);
	function insert_highlight($text);
	function insert_form($form);
	function insert_table($ttab);
}
?>
