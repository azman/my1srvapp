<?php
/**
 * ThemeNone.php
 * - non-theme... using purely html
**/
require_once dirname(__FILE__).'/Theme.php';
require_once dirname(__FILE__).'/ThemeI.php';
class ThemeNone extends Theme implements ThemeI {
	function __construct($page=null) {
		parent::__construct($page);
	}
	// form stuffs
	function create_form($uuid) {
		$form = new HTMLForm($uuid,'work.php');
		return $form;
	}
	function create_form_input_text($form,$flbl,$name,$opts=[]) {
		if (array_key_exists("uuid",$opts)) $uuid = $opts['uuid'];
		else $uuid = null;
		if (array_key_exists("tval",$opts)) $tval = $opts['tval'];
		else $tval = null;
		$pick = [];
		if (array_key_exists("hold",$opts)) $pick['hold'] = $opts['hold'];
		if (array_key_exists("ro",$opts)) $pick['ro'] = $opts['ro'];
		$temp = $form->make_input_text($flbl,$name,$uuid,$tval,$pick);
		$temp->insert_style("width:100%;");
		$temp->insert_linebr(2);
		return $temp;
	}
	function create_form_input_pass($form,$flbl,$name,$opts=[]) {
		if (array_key_exists("uuid",$opts)) $uuid = $opts['uuid'];
		else $uuid = null;
		if (array_key_exists("tval",$opts)) $tval = $opts['tval'];
		else $tval = null;
		$pick = [];
		if (array_key_exists("hold",$opts)) $pick['hold'] = $opts['hold'];
		if (array_key_exists("ro",$opts)) $pick['ro'] = $opts['ro'];
		$temp = $form->make_input_pass($flbl,$name,$uuid,$tval,$pick);
		$temp->insert_style("width:100%;");
		$temp->insert_linebr(2);
		return $temp;
	}
	function create_form_input_hidden($form,$name,$uuid,$vval) {
		$temp = $form->make_input_hidden($name,$uuid,$vval);
		return $temp;
	}
	function create_form_submit($form,$text,$name) {
		$temp = $form->make_input_submit($text,$name);
		$temp->insert_linebr();
		return $temp;
	}
	function create_form_input_radio($form,$text,$name,$opts=[]) {
		if (array_key_exists("uuid",$opts)) $uuid = $opts['uuid'];
		else $uuid = null;
		if (array_key_exists("vval",$opts)) $vval = $opts['vval'];
		else $vval = null;
		if (array_key_exists("chkd",$opts)) $chkd = $opts['chkd'];
		else $chkd = false;
		$temp = $form->make_input_radio($text,$name,$uuid,$vval,$chkd);
		if (array_key_exists("linebr",$opts)) {
			$pick = intval($opts['linebr']);
			if ($pick>0) $temp->insert_linebr($pick);
		}
		return $temp;
	}
	function create_form_input_radio_group($form,$flbl,$name,$list,$opts=[]) {
		$what = $this->create_form_label($form,$flbl);
		if (array_key_exists("lbrk",$opts)&&$opts['lbrk']===true)
			$what->insert_linebr();
		//  [ label, value ]
		$loop = 0; $temp = null; $that = -1;
		if (array_key_exists("rsel",$opts))
			$that = intval($opts['rsel']);
		foreach ($list as $item) {
			$pick = [];
			$pick['vval'] = $item[1];
			$chkd = ($loop===$that) ? true : false;
			if ($chkd) $pick['chkd'] = $chkd;
			$temp = $this->create_form_input_radio($form,$item[0],$name,$pick);
			$loop++;
		}
		if ($temp!=null) {
			if (array_key_exists("linebr",$opts)) {
				$pick = intval($opts['linebr']);
				if ($pick>0) {
					$rlbl = $temp->get_label();
					$rlbl->insert_linebr($pick);
				}
			}
		}
	}
	function create_form_select($form,$flbl,$name,$list,$opts=[]) {
		$what = $this->create_form_label($form,$flbl);
		if (array_key_exists("lbrk",$opts)&&$opts['lbrk']===true)
			$what->insert_linebr();
		if (array_key_exists("uuid",$opts)) $uuid = $opts['uuid'];
		else $uuid = null;
		$temp = $form->make_select($name,$uuid,$list);
		$temp->insert_linebr(2);
		return $temp;
	}
	function create_form_select_option($form,$psel,$text,$name,$chkd=false) {
		$temp = $form->make_select_option($psel,$text,$name,$chkd);
		return $temp;
	}
	function create_form_input_file($form,$flbl,$name,$opts=[]) {
		if (array_key_exists("uuid",$opts)) $uuid = $opts['uuid'];
		else $uuid = null;
		$temp = $form->make_input_file($flbl,$name,$uuid);
		$temp->insert_linebr(2);
		return $temp;
	}
	function create_form_input_checkbox($form,$flbl,$name,$opts=[]) {
		if (array_key_exists("uuid",$opts)) $uuid = $opts['uuid'];
		else $uuid = $name;
		if (array_key_exists("chkd",$opts)) $chkd = $opts['chkd'];
		else $chkd = false;
		$temp = $form->make_input_checkbox($flbl,$name,$uuid,$chkd);
		if (array_key_exists("linebr",$opts)) {
			$pick = intval($opts['linebr']);
			if ($pick>0) {
				$ilbl = $temp->get_label();
				$ilbl->insert_linebr($pick);
			}
		}
		return $temp;
	}
	function create_form_label($form,$flbl,$opts=[]) {
		$temp = $form->make_label($flbl,false);
		if (array_key_exists("linebr",$opts)) {
			$pick = intval($opts['linebr']);
			if ($pick>0) $temp->insert_linebr($pick);
		}
		return $temp;
	}
	function create_form_textarea($form,$flbl,$name,$opts=[]) {
		$what = $this->create_form_label($form,$flbl);
		if (array_key_exists("lbrk",$opts)&&$opts['lbrk']===true)
			$what->insert_linebr();
		$temp = $form->make_textarea($name,null);
		$temp->insert_style("resize:none;width:100%;");
		if (array_key_exists("linebr",$opts)) {
			$pick = intval($opts['linebr']);
			if ($pick>0) $temp->insert_linebr($pick);
		}
		return $temp;
	}
	// table stuffs
	function create_table() {
		$ttab =  new HTMLTable();
		$ttab->insert_keyvalue('width','100%',true);
		$ttab->insert_keyvalue('cellpadding','10',true);
		$ttab->insert_keyvalue('border','1',true);
		return $ttab;
	}
	function create_table_header_row(&$ttab=null) {
		if ($ttab===null) $ttab = $this->create_table();
		$trow = $ttab->insert_header_row();
		return $trow;
	}
	function create_table_header_col(&$ttab=null) {
		if ($ttab===null) $ttab = $this->create_table();
		return $ttab->insert_header_col();
	}
	function create_table_data_row(&$ttab=null) {
		if ($ttab===null) $ttab = $this->create_table();
		$trow = $ttab->insert_data_row();
		return $trow;
	}
	function create_table_data_col(&$ttab=null) {
		if ($ttab===null) $ttab = $this->create_table();
		return $ttab->insert_data_col();
	}
	//---- Interface methods
	function create_panel($item) {
		$temp = $this->_page->make_div(false);
		$temp->do_multiline();
		if (is_a($item,'HTMLObject')) $temp->append_object($item);
		else $temp->insert_inner($item); // assume text
		return $temp;
	}
	function create_menu_item($link,$text,$type=MENUITEM_LINK) {
		if ($type===MENUITEM_CMD_)
			$link = $this->_page->text_command($link);
		$item = $this->_page->make_link($link,$text);
		$temp = $this->_page->make_span("[ ".$item->form_html()." ]",false);
		$temp->insert_style("white-space:nowrap;");
		return $temp;
	}
	function create_badge($show,$opts=[]) {
		$temp = $this->_page->make_span("(".$show.")");
		return $temp;
	}
	function insert_object($item) {
		if (is_a($item,'HTMLObject')) $temp = $item;
		else $temp = $this->_page->make_para($item); // assume text?
		$this->_page->append_2body($temp);
		return $temp;
	}
	function insert_page_title($title=null) {
		$text = $this->_page->text_page_title($title);
		$temp = $this->_page->make_hobject($text,'h1');
		$temp->do_1skipline();
		$this->insert_object($temp);
		return $temp;
	}
	function insert_page_section($title) {
		$temp = $this->_page->make_hobject($title,'h2');
		$temp->do_1skipline();
		$this->insert_object($temp);
		return $temp;
	}
	function insert_block($item) {
		$temp = $this->create_panel($item);
		$this->insert_object($temp);
		return $temp;
	}
	function insert_menu($list,$user=false) {
		if (!is_array($list))
			$this->throw_this("No array for menu!");
		$menu = "";
		foreach ($list as $item) {
			$temp = $this->create_menu_item($item['that'],
				$item['text'],$item['type']);
			$link = $temp->form_html();
			if (strlen($menu)>0) $menu = $menu."&nbsp;&nbsp;\n".$link;
			else $menu = $link;
		}
		if ($user!==false) {
			// $user should be output of dodata->getProfile() or null
			if ($user===null) $text = 'Hello, World!';
			else $text = 'Hello, '.$user['nick'];
			$menu = $text."&nbsp;&nbsp;\n".$menu;
		}
		return $this->insert_object($menu);
	}
	function insert_highlight($text) {
		return $this->insert_object("<b>$text</b>");
	}
	function insert_form($form,$opts=[]) {
		$temp = $this->insert_block($form);
		$size = 25;
		if (array_key_exists('fwpct',$opts)) {
			$size = intval($opts['fwpct']);
			if ($size<=0||$size>100)
				$size = 25;
		}
		$temp->insert_style("width:".$size."%;padding:1em;");
		$temp->insert_style("border-width:1px;border-style:solid;");
		return $temp;
	}
	function insert_table($ttab) {
		$temp = $this->insert_block($ttab);
		return $temp;
	}
}
?>
